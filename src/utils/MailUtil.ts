import Mailgun from "mailgun.js";
import formData from "form-data";

export default class MailUtil {
  static async send(from: string, html: string) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

    const apiKey = process.env.MAILGUN_API_KEY;
    const domain = process.env.MAILGUN_DOMAIN;

    const mailgun = new Mailgun(formData);
    const client = mailgun.client({ username: "api", key: apiKey, url: "https://api.eu.mailgun.net" });

    var data = {
      from: from,
      to: process.env.CONTACT_EMAIL,
      subject: 'Bericht van pingpingapp.nl',
      html: html,
    };

    client.messages.create(domain, data)
      .then((res: any) => {
        console.log(res);
      })
      .catch((err: any) => {
        console.log(err);
      })
      .finally(() => {
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = '1';
      });
  }
}
