# PingPing Backend &middot; [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](CONTRIBUTING.md) &middot; [![PingPing Native](https://img.shields.io/badge/PingPing-Native-brightgreen.svg)](https://gitlab.com/trimm-digital-craftmanship/pingping/pingping-native)

## Security

If you see or suspect any vulnerabilities please read the [security policy](SECURITY.md) in order to report them.

## Development

First read [contributing](CONTRIBUTING.md)

## Setting up the project locally

In order to get the project up and running you will need 
- The latest version of node.js
- The latest version of yarn
- A mongoDB server

If you don't have a mongoDB server set up locally, there is also a docker-compose.yml file under `docker-dev` that you can spin up to get a mongoDB server up and running. Of course, you will need docker for this:

```
cd docker-dev
docker compose -p pingping-backend up -d
```

Once you have the database up and running, you can seed it with mock data by running `yarn seed`

The server reads the environment variable PORT to get its port, so you should set it beforehand. The default we use is 4010. Once that is done, you can then run the server:

```
PORT=4010 yarn dev
```

## Admin Development

The admin panel is written as a separate vue application. We highly recommend getting it running, as it allows you to have an overview of things like users, as well as perform some tasks like sending notifications. To get the admin panel running, you can:

```
cd admin
yarn install
VUE_APP_GRAPHQL_HTTP=http://localhost:4010/api yarn serve
```

The VUE_APP_GRAPHQL_HTTP should point to your local backend, so make sure the port matches with the one you used above.

## Deployment

If you want to test a deployment build, you can:

```
yarn build
yarn prod
```

## Tests

To run tests, you can:

```
yarn test
```

## Enums

### TaskType

Tasks have 3 different types:

#### DateOfBirth

expected value of answer when updating task is `1996-12-31`

#### YesOrNo

expected value of answer when updating task is `yes` or `no`

#### MultipleChoices

expected value of answer comma separated values `choiceOne,choiseTwo`

### TaskStatus

#### Completed

Completed by the user

## Dismissed

Dismissed by the user, that is, the task completion resulted in a negative response. This will not cause a linked task to be completed on a route.

#### PendingUser

Waiting for action by the user

### RewardStatus

#### AvailableToClaim

The reward can be claimed by the user and he has sufficient balance

#### Claimed

The reward is claimed by the user but not used

#### ClaimedAndUsed

The reward is claimed and used by the user

#### Expired

The reward is expired, cannot be claimed or used

## Configuration

### Initial Data

The `json` files in `defs` contain the configuration for the application and tenants.

#### onboardingTasks

Defines the initial tasks the user is asked to perform before being assigned his initial route. The tasks can have a direct link to a `routeTask`, so when completed this will also complete the task on that linked route.

#### routes

Available routes and their definitions of tasks

#### rewards

Available rewards for purchase

#### achievements

Unlockable achievements

## Error handling

Errors can be return on a specific occasion, the error message will be descriptive enough to clarify the meaning of the error. Here are some of these error listed

### task_not_defined

The task requested doesn't exists in the definition files

### task_not_found_on_user

The task requested doesn't exist on the user. This means it hassn't been assigned to the user or does not exist on a route the user has assigned to him.

### task_invalid_status

The task has an invalid status on the user. This could mean the task is already completed.

### achievement_not_defined

The achievement doesn't exist in the definition files

### reward_not_defined

The reward doesn't exist in the definition files

### route_not_defined

The route doesn't exist in the definition files

## Adding new cities

- Add the city's entry to defs/none/onboarding.json
- Create the definition for the new city by creating a directory under defs with the city's name
	- Check defs/ermelo for an example of how this works
	- Make sure to create files for achievements, onboarding, rewards, and routes, even if they are empty
- Add whatever routes, achievements and rewards are relevant to the city
- Register the new city option by adding it to the available tenants list
	- Change the `TENANTS` variable under `src/utils/InitialDataUtil.ts`

Note that this will also create an account for the new tenant.
